#!/bin/bash
#1.设置网卡名称，比如：eth0、em1
eth=$1
#2.设置显示上传速度还是下载速度，单位KB，上传:upload,下载:download
show=$2

if [ $# != 2 ] ;then
echo Usage:/bin/bash $0 "<网卡名称> <upload|download>"
exit 500
fi

#####下面不清楚就不要改动#######
R1=`cat /sys/class/net/${eth}/statistics/rx_bytes`
T1=`cat /sys/class/net/${eth}/statistics/tx_bytes`
sleep 2
R2=`cat /sys/class/net/${eth}/statistics/rx_bytes`
T2=`cat /sys/class/net/${eth}/statistics/tx_bytes`
TBPS=`expr $T2 - $T1`
RBPS=`expr $R2 - $R1`
uploadKB=`expr $TBPS / 1024`
downloadKB=`expr $RBPS / 1024`
case $show in
  upload)
  echo -n ${uploadKB}
  ;;
  download)
  echo -n ${downloadKB}
  ;;
esac